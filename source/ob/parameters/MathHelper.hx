package ob.parameters;

class MathHelper {
	public static function clampInt(n:Int, min:Int, max:Int):Int {
		return n < min ? min : n > max ? max : n;
	}

	public static function clampFloat(n:Float, min:Float, max:Float):Float {
		return n < min ? min : n > max ? max : n;
	}
}
