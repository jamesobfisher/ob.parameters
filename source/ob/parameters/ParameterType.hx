package ob.parameters;

enum ParameterType {
	NONE;
	NOTE;
	VELOCITY;
	CC;
	NRPN;
}
