package ob.parameters;
using ob.parameters.MathHelper;

class LimitedValue {
	public var min(default, null):Int;
	public var max(default, null):Int;
	public var def(default, null):Int;
	@:isVar public var now(get, set):Int;
	public var percent(default, set):Float;

	function get_now() {
		return now;
	}

	function set_now(v:Int):Int {
		now = v.clampInt(min, max);
		return now;
	}

	function set_percent(v:Float):Float {
		percent = v.clampFloat(-1.0, 1.0);
		var range = max - min;
		var percentOfRange = percent * range;
		now = Math.round(min + percentOfRange);
		return percent;
	}

	public function new(minimumValue:Int, maximumValue:Int, ?defaultValue:Int, ?currentValue:Int) {
		min = minimumValue;
		max = maximumValue;
		def = defaultValue == null ? min : defaultValue;
		now = currentValue == null ? def : currentValue;
	}
}
