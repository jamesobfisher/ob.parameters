package ob.parameters;

class Parameter extends LimitedValue {
	public function new(name:String, id:Int, type:ParameterType, minimumValue:Int=0, maximumValue:Int=127, ?defaultValue:Int, ?currentValue:Int) {
		this.name = name;
		this.id = id;
		this.type = type;
		super(minimumValue, maximumValue, defaultValue, currentValue);
	}

	public var name(default, null):String;
	public var id(default, null):Int;
	public var type(default, null):ParameterType;
}
